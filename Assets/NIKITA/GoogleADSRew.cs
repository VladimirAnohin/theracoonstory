﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using InitScriptName;
using System;

public class GoogleADSRew : MonoBehaviour {
    private BannerView bannerView;
    private RewardBasedVideoAd rewardBasedVideo;
    InterstitialAd interstitial;
    public static GoogleADSRew THIS;
    public int adsIndex;
    [Tooltip("Через сколько раз показывать объявления?")]
    public int loadForAds;

    private void Awake()
    {   THIS = this;
        Debug.LogError("Awake");
    }
    private void Start()
    {

        Debug.LogError("Start2");

        string appId = "ca-app-pub-1134646820220559~6128960823"; //Апп айди

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);


        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;


        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        this.RequestRewardedVideo();

        if (mainscript.Instance && mainscript.Instance.currentLevel >= 5)
        {
            if (PlayerPrefs.GetInt("levelAds", 0) >= loadForAds)
            {
                RequestInterstitial();
            }
        }
    }

    private void HandleOnAdClosed(object sender, EventArgs e)
    {
        RequestInterstitial();
    }

    // Update is called once per frame
    void Update () {

    }
    void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        //     print("On admob reward loaded " + args);
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        //       MonoBehaviour.print(
        //          "HandleRewardBasedVideoRewarded event received for "
        //                        + amount.ToString() + " " + type);
        switch (adsIndex)
        {
            case 1:
                UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
                break;
            case 2:
                InitScript.Gems += 10;
                PlayerPrefs.SetInt("Gems", InitScript.Gems);
                PlayerPrefs.Save();
                break;
            case 3:
                InitScript.Lifes += 1;
                PlayerPrefs.SetInt("Lifes", InitScript.Lifes);
                PlayerPrefs.Save();
                break;
        }

        RequestRewardedVideo();
    }
    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        RequestRewardedVideo();
    }
    private void RequestRewardedVideo()
    {

        string adUnitId = "ca-app-pub-1134646820220559/3927965597"; //С наградой


        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, adUnitId);
    }
    private void RequestInterstitial()
    {
        Debug.LogError("REQU_I");
        interstitial = new InterstitialAd("ca-app-pub-1134646820220559/3891642237");//Межстраничка

        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }
    public void StartADS()
    {
        Debug.LogError(InitScript.Gems + ";" + InitScript.Lifes);

        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
        else
        {
        }

    }
    public void StartSADS()
    {
        Debug.LogError("StartSADS");
        if (interstitial != null && interstitial.IsLoaded())
        {
            loadForAds--;
            interstitial.Show();
        }
        else
        {
            Debug.LogError("isNotLoad");
        }
    }
}
