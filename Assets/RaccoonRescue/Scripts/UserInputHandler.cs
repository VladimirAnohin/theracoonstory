﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UserInputHandler : MonoBehaviour
{
    public UnityEvent eventForEraise;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            eventForEraise.Invoke();
        }
    }
}
