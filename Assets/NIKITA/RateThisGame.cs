﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateThisGame : MonoBehaviour
{
    [Tooltip("Отсрочка сообщения о предложении оценить игру, в часах")]
    public int hours;
    [Tooltip("Отсрочка сообщения о предложении оценить игру, в заходах")]
    public int enterAmount;

    [Tooltip("Ссылка на приложение")]
    public string GooglePlayURL;
    // Use this for initialization
    void Awake()
    {
        if(PlayerPrefs.GetInt("rated", 0) == 1)
        {
            gameObject.SetActive(false);
            return;
        }

        int playerEnterAmount = PlayerPrefs.GetInt("nextRate", 1);

        if (playerEnterAmount < enterAmount)
        {
            gameObject.SetActive(false);
        }

        /* Старый метод отсчета предложения оценить, в часах
        if (PlayerPrefs.HasKey("nextRate"))
        {
            Debug.LogError(new System.DateTime(long.Parse(PlayerPrefs.GetString("nextRate"))));
            Debug.LogError(System.DateTime.UtcNow.AddHours(-hours));
            if (long.Parse(PlayerPrefs.GetString("nextRate")) < System.DateTime.UtcNow.AddHours(-hours).Ticks)
            {

            }
            else
            {
       
                gameObject.SetActive(false);
            }
        }
        else
        {
            PlayerPrefs.SetString("nextRate",System.DateTime.UtcNow.Ticks.ToString());
                gameObject.SetActive(false);
        }*/

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Ok()
    {
        Application.OpenURL(GooglePlayURL);
        PlayerPrefs.SetInt("rated", 1);
        GetComponent<AnimationManager>().CloseMenu();
    }
    public void Cancel()
    {
        GetComponent<AnimationManager>().CloseMenu();
        PlayerPrefs.SetInt("nextRate", 0);
    }
}
