﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserEnterCounter : MonoBehaviour
{
    private void OnApplicationPause(bool pause)
    {
        int playerEnterAmount = PlayerPrefs.GetInt("nextRate", 1);

        PlayerPrefs.SetInt("nextRate", ++playerEnterAmount);
    }
}
