﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_ADS
using UnityEngine.Advertisements;
#endif
using UnityEngine.SceneManagement;
using InitScriptName;

#if GOOGLE_MOBILE_ADS
using GoogleMobileAds.Api;
#endif
public class RewardedVideo : MonoBehaviour {
	public VideoWatchedEvent videoWatchedEvent;
	string rewardedVideoZone;
	// Use this for initialization
	void OnEnable () {
		if (GetRewardedAdsReady ()) {
			gameObject.SetActive (true);
		} else {
			gameObject.SetActive (false);
		}

	}

	public void GetCoins (int addCoins) {
		RewardIcon reward = MenuManager.Instance.RewardPopup.GetComponent<RewardIcon> ();
		reward.SetIconSprite (0);
		reward.gameObject.SetActive (true);
		InitScript.Instance.AddGems (addCoins);
		MenuManager.Instance.MenuCurrencyShop.GetComponent<AnimationManager> ().CloseMenu ();
	}

	public void GetLifes () {
		RewardIcon reward = MenuManager.Instance.RewardPopup.GetComponent<RewardIcon> ();
		reward.SetIconSprite (1);
		reward.gameObject.SetActive (true);
		InitScript.Instance.RestoreLifes ();
		MenuManager.Instance.MenuLifeShop.GetComponent<AnimationManager> ().CloseMenu ();

	}

	public void ContinuePlay () {
		MenuManager.Instance.PreFailedBanner.GetComponent<AnimationManager> ().GoOnFailed ();
	}

	public void ShowRewardedAds () {
#if UNITY_ADS
		if (GetRewardedAdsReady ()) {
			Advertisement.Show (rewardedVideoZone, new ShowOptions {
				resultCallback = result => {
					if (result == ShowResult.Finished) {
						CheckRewardedAds ();
					}
				}
			});
		}
#endif
#if GOOGLE_MOBILE_ADS
		ShowAdmobRewarded ();
#endif
	}

	void CheckRewardedAds () {
		videoWatchedEvent.Invoke (1);
		//		RewardIcon reward = null;
		//		if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("map"))//TODO: set reward window to Menu manager
		//			reward = GameObject.Find ("Canvas").transform.Find ("Reward").GetComponent<RewardIcon> ();
		//		if (currentReward == RewardedAdsType.GetGems) {
		//			reward.SetIconSprite (0);
		//
		//			reward.gameObject.SetActive (true);
		//			InitScript.Instance.AddGems (LevelEditorBase.THIS.rewardedGems);
		//			GameObject.Find ("CanvasMenu").transform.Find ("GemsShop").GetComponent<AnimationManager> ().CloseMenu ();
		//		} else if (currentReward == RewardedAdsType.GetLifes) {
		//			reward.SetIconSprite (1);
		//			reward.gameObject.SetActive (true);
		//			InitScript.Instance.RestoreLifes ();
		//			GameObject.Find ("Canvas").transform.Find ("LiveShop").GetComponent<AnimationManager> ().CloseMenu ();
		//		} else if (currentReward == RewardedAdsType.GetGoOn) {
		//			GameObject.Find ("CanvasMenu").transform.Find ("PreFailedBanner").GetComponent<AnimationManager> ().GoOnFailed ();
		//		}

	}

	public bool GetRewardedAdsReady () {
#if UNITY_ADS
		rewardedVideoZone = "rewardedVideo";
		if (Advertisement.IsReady (rewardedVideoZone)) {
			return true;
		} else {
			rewardedVideoZone = "rewardedVideoZone";
			if (Advertisement.IsReady (rewardedVideoZone)) {
				return true;
			}
		}
#elif GOOGLE_MOBILE_ADS
		if (AdsEvents.THIS.admobRewardedVideo.IsLoaded ())
			return true;

#endif

		return false;
	}


	#if GOOGLE_MOBILE_ADS
	
	private void ShowAdmobRewarded () {
		if (AdsEvents.THIS.admobRewardedVideo.IsLoaded ()) {
			AdsEvents.THIS.admobRewardedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
			AdsEvents.THIS.admobRewardedVideo.Show ();
		}
	}

	public void HandleRewardBasedVideoRewarded (object sender, Reward args) {
		CheckRewardedAds ();
		AdsEvents.THIS.admobRewardedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
		string type = args.Type;
		double amount = args.Amount;
		print ("User rewarded with: " + amount.ToString () + " " + type);
	}
	#endif

}

[System.Serializable]
public class VideoWatchedEvent : UnityEvent<int> {
}